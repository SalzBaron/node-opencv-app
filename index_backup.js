const cv = require('opencv4nodejs');
const express = require('express');
const path = require('path');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const FPS = 10;
const wCap= new cv.VideoCapture(0);
wCap.set(cv.CAP_PROP_FRAME_WIDTH, 320);
wCap.set(cv.CAP_PROP_FRAME_HEIGHT, 320);

app.use(express.static(path.join(__dirname, "./")));

setInterval(() => {
    
    //matchImages().catch( err => console.log(err));
    doMatch();
}, 1000/FPS );

const matchImages = async() => {

    const originalMat = wCap.read();
    const referenceMat = await cv.imreadAsync(`./images/marker23.jpg`);

    const matched = originalMat.matchTemplate(referenceMat, 5);

    const minMax = matched.minMaxLoc();
    const {maxLoc: {x, y} } = minMax;

    originalMat.drawRectangle(
        new cv.Rect(x, y, referenceMat.cols, referenceMat.rows),
        new cv.Vec(0, 255, 0),
        2,
        cv.LINE_8
    );

    const image = cv.imencode('.jpg', originalMat).toString('base64');
    io.emit('image', image);

};

const matchFeatures = ({originalMat, referenceMat, detector, matchFunc}) => {
    // detect kp
    const keyPoints1 = detector.detect(originalMat);
    const keyPoints2 = detector.detect(referenceMat);

    // compute feature descriptors
    const descriptors1 = detector.compute(originalMat, keyPoints1);
    const descriptors2 = detector.compute(referenceMat, keyPoints2);

    // match feature descriptors
    const matches = matchFunc(descriptors1, descriptors2);

    const bestN = 20;
    const bestMatches = matches.sort((match1, match2) => match1.distance - match2.distance).slice(0,bestN);

    console.log(keyPoints1[0].point.x);


    cv.minEnclosingCircle(keyPoints1[0].point, 30);
    // originalMat.drawRectangle(
    //     new cv.Rect(keyPoints1[0].point.x - 20, keyPoints1[0].point.y -20, referenceMat.cols, referenceMat.rows),
    //     new cv.Vec(0, 255, 0),
    //     2,
    //     cv.LINE_8
    // );

    return cv.drawMatches(originalMat, referenceMat, keyPoints1, keyPoints2, bestMatches);
};

const doMatch = () => {
    // ORB matching
    let originalMat = wCap.read();
    originalMat = originalMat.cvtColor(cv.COLOR_BGR2GRAY, 0);
    const referenceMat = cv.imread(`./images/marker23.jpg`);

    
    const orbMatchesImg = matchFeatures( {
        originalMat,
        referenceMat,
        detector: new cv.ORBDetector(),
        matchFunc: cv.matchBruteForceHamming
    });
    

    // BFMatcher matching
    /*
    const bf = new cv.BFMatcher(cv.NORM_L2, true);
    const orbBFMatchImg = matchFeatures({
    originalMat,
    referenceMat,
    detector: new cv.ORBDetector(),
    matchFunc: (desc1, desc2) => bf.match(desc1, desc2)
    });
    */
    

   const image = cv.imencode('.jpg', orbMatchesImg).toString('base64');
   io.emit('image', image);

}

server.listen(8000);