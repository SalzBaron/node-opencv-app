const cv = require('opencv4nodejs');
const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

app.use(express.static(path.join(__dirname, './public')));

//const FPS = 10;
//const wCap= new cv.VideoCapture(0);
//wCap.set(cv.CAP_PROP_FRAME_WIDTH, 320);
//wCap.set(cv.CAP_PROP_FRAME_HEIGHT, 320);

let referenceMat = cv.imread('./images/marker23.jpg');

/*
setInterval(() => {
  doMatch();
}, 1000 / FPS);
*/

const matchFeatures = ({ referenceMat, originalMat, detector, matchFunc }) => {
    // detect keypoints
    const keyPoints1 = detector.detect(referenceMat);
    const keyPoints2 = detector.detect(originalMat);
  
    // compute feature descriptors
    const descriptors1 = detector.compute(referenceMat, keyPoints1);
    const descriptors2 = detector.compute(originalMat, keyPoints2);
  
    // match the feature descriptors
    const matches = matchFunc(descriptors1, descriptors2);
  
    // only keep good matches
    const bestN = 50;
    let bestMatches = matches.sort(
      (match1, match2) => match1.distance - match2.distance
    ).slice(0, bestN);

    let b = bestMatches.filter(e => e.distance < 300);

    //console.log(b);

    if (b.length > 10){
    let tp = [];
    let qp = [];
    let pts = [
      new cv.Point2(0, 0), new cv.Point2(0, referenceMat.rows), new cv.Point2(referenceMat.cols, referenceMat.rows), new cv.Point2(referenceMat.cols, 0)
    ]

    const matData = [
      pts.map(pt => [pt.x, pt.y])
    ];
    const src = new cv.Mat(matData, cv.CV_32FC2);

    bestMatches.forEach( m => {
        tp.push(keyPoints2[m.trainIdx].point);
        qp.push(keyPoints1[m.queryIdx].point);
    })

    let hom = cv.findHomography(qp, tp, cv.RANSAC, 3.0);
    const result = src.perspectiveTransform(hom.homography);
    let [data]= result.getDataAsArray();

    let pts3 = [];
    data.forEach(e => {
      pts3.push(new cv.Point2(e[0], e[1]))
    })

    originalMat.drawPolylines([pts3], true, new cv.Vec(0,255, 0), 5);
  }

    return originalMat;
  };

  doMatch = ({camMat}) => {
    let originalMat = camMat;

    // check if opencv compiled with extra modules and nonfree
    if (cv.xmodules.xfeatures2d) {
      const siftMatchesImg = matchFeatures({
        referenceMat,
        originalMat,
        detector: new cv.SIFTDetector({ nFeatures: 1000 }),
        matchFunc: cv.matchFlannBased
      });

      //cv.imshowWait('SIFT matches', siftMatchesImg);
      const image = cv.imencode('.jpg', siftMatchesImg).toString('base64');
      io.emit('image', image);
    } else {
      console.log('skipping SIFT matches');
    }
  };

  io.on('connection', socket => {
    console.log(socket.id);
  

    socket.on('frame', data => {
        doMatch(cv.imdecode(data));
    });


  });

server.listen(8000, () => {
    console.log("Server running");
})